package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // changed it to canvas from null
		this.paint = paint; // changed it to paint from null
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {

		paint.setColor(c.getColor());
		if (c.getShape() instanceof Polygon)
		{
			onPolygon((Polygon)c.getShape());
			paint.setColor(c.getColor());
		}
		return null;
	}

	@Override
	public Void onFill(final Fill f) {

		paint.setStyle(Style.FILL_AND_STROKE);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		float x =0;
		float y=0;
		for (Shape s : g.getShapes()) {
			if (s instanceof Location)
			{
				Shape shape = ((Location) s).getShape();
				x = ((Location) s).getX();
				y = ((Location) s).getY();
				canvas.translate(x,y);

				if (shape instanceof Circle)
				{
					onCircle((Circle)shape);
				}
				else if (shape instanceof Rectangle)
				{
					onRectangle((Rectangle)shape);
				}
				else if (shape instanceof Stroke)
				{
					onStroke((Stroke)shape);
					Shape s2 = ((Stroke) shape).getShape();
					if (s2 instanceof Polygon)
					{
						onPolygon((Polygon)s2);
					}
					else if (s2 instanceof Outline)
					{
						onOutline((Outline)s2);
						paint.setStyle(Style.STROKE);
						paint.setColor(3);
					}
					else if (s2 instanceof Fill)
					{
						onFill((Fill)s2);
						Group group =  (Group)((Fill) s2).getShape();
						onGroup(group);

					}

				}
				canvas.translate(-x,-y);
				paint.setStyle(Style.STROKE);
				paint.setColor(3);
			}
			if (s instanceof Circle)
			{
				onCircle((Circle)s);
			}
			else if (s instanceof Rectangle)
			{
				onRectangle((Rectangle)s);
			}
			else if (s instanceof Outline){

				onOutline((Outline) s);
			}
			else if (s instanceof Polygon)
			{
				onPolygon((Polygon)s);

			}
			else if (s instanceof Stroke)
			{
				onStroke(((Stroke) s));
				Outline out = new Outline(((Stroke) s).getShape());
				onOutline(out);
				onStroke(((Stroke) s));


			}

		}

		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		if (l.getShape() instanceof Group)
		{
			canvas.translate(l.getX(),l.getY());
			onGroup((Group)l.getShape());
			canvas.translate(-l.getX(),-l.getY());
			return null;
		}
		//if(l.getShape() instanceof )
		Rectangle r = (Rectangle) l.getShape();
		canvas.translate(l.getX(),l.getY());
		canvas.drawRect(0,0,r.getWidth(),r.getHeight(),paint);
		canvas.translate(-l.getX(),-l.getY());

		return null;
	}


	@Override
	public Void onRectangle(final Rectangle r) {

		canvas.drawRect(0,0,r.getWidth(),r.getHeight(),paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		paint.setStyle(Style.STROKE);
		Shape shape = o.getShape();
		if (shape instanceof Rectangle)
		{
			onRectangle((Rectangle)shape);
		}
		else if (shape instanceof Circle)
		{
			onCircle((Circle)shape);
		}
		else if (shape instanceof Polygon)
		{

			onPolygon((Polygon)shape);
			int hi=4;
		}


		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		int size = s.getPoints().size()*4;
		final float[] pts=new float[size];
		int count=0;
		for (Point point : s.getPoints()){
			int x = point.getX();
			int y = point.getY();
			pts[count]=x;
			count+=1;
			pts[count]=y;
			count+=1;
			//need to close the polygon
		}
		pts[count]=pts[0];
		count+=1;
		pts[count]=pts[1];
		count+=1;
		pts[count]=pts[6];
		count+=1;
		pts[count]=pts[7];
		count+=1;
		pts[count]=pts[2];
		count+=1;
		pts[count]=pts[3];
		count+=1;
		pts[count]=pts[4];
		count+=1;
		pts[count]=pts[5];



		canvas.drawLines(pts, paint);
		return null;

	}
}
