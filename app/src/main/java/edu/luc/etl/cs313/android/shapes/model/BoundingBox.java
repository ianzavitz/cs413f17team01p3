package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Rect;

import java.util.List;

import static edu.luc.etl.cs313.android.shapes.model.Fixtures.complexGroup;
import static edu.luc.etl.cs313.android.shapes.model.Fixtures.simplePolygon;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));

	}

	@Override
	public Location onFill(final Fill f) {
		Shape shape = f.getShape();
		//return new Location(0,0,new Rectangle(r.getWidth(),r.getHeight()));
		return new Location(0,0,shape);
	}

	@Override
	public Location onGroup(final Group g) {

		int f = 0;
		int minLx =0;
		int minLy =0;
		int maxRx=0;
		int maxRy=0;
		int topLx=0;
		int topLy = 0;
		int botLx = 0;
		int botLy= 0;
		for (Shape s : g.getShapes()) {

			Location l = s.accept(this);

			Shape shape = l.getShape();
			int x = l.getX();
			int y = l.getY();

			if (shape instanceof Circle || s instanceof Circle )
			{
				int r=0;
				if (shape instanceof Circle) {
					 r= ((Circle) shape).getRadius();
				}
				 topLx = x-r;
				 topLy = y-r;
				 botLx = x+r;
				 botLy= y+r;
				if (f==0)
				{
					minLx=topLx;
					minLy=topLy;
					maxRx=botLx;
					maxRy=botLy;
					f+=1;
				}
				else
				{
					if (minLx>topLx)
					{
						minLx=topLx;
					}
					if (minLy>topLy)
					{
						minLy=topLy;
					}
					if (maxRx<botLx)
					{
						maxRx=botLx;
					}
					if (maxRy<botLy)
					{
						maxRy =botLy;
					}
				}
			}
			else if (shape instanceof Rectangle)
			{
				int h = ((Rectangle) shape).getHeight();
				int w = ((Rectangle) shape).getWidth();
				topLx=x;
				topLy=y;
				botLx = x +w;
				 botLy = y +h;

				if (f==0)
				{
					minLx=x;
					minLy=y;
					maxRx=botLx;
					maxRy=botLy;
					f+=1;
				}
				else
				{
					if (minLx>topLx)
					{
						minLx=topLx;
					}
					if (minLy>topLy)
					{
						minLy=topLy;
					}
					if (maxRx<botLx)
					{
						maxRx=botLx;
					}
					if (maxRy<botLy)
					{
						maxRy =botLy;
					}
				}

			}
			else if (shape instanceof Polygon || s instanceof Polygon)
			{
				List<? extends Point> points= ((Polygon) shape).getPoints();
				for (int i =0;i<points.size();i+=1)
				{
					x =points.get(i).getX();
					y = points.get(i).getY();

						if(minLx > points.get(i).getX())
						{
							minLx = points.get(i).getX();
						}
						if(minLy > y)
						{
							minLy = points.get(i).getY();
						}
						if(maxRx<points.get(i).getX())
						{
							maxRx=points.get(i).getX();
						}
						if (maxRy<points.get(i).getY())
						{
							maxRy=points.get(i).getY();
						}

				}
			}
			else if (shape instanceof Stroke){ //shape is a FIll or Polygon, or Outline
				Shape tester = ((Stroke) shape).getShape();
				if(tester instanceof Polygon)
				{
					Location loct= onPolygon((Polygon)shape);
					if (loct.getX()<minLx)
					{
						minLx = loct.getX();
					}
					if (loct.getY()<minLy)
					{
						minLx= loct.getY();
					}
					Shape rect=loct.getShape();
					int w= ((Rectangle)rect).getWidth() + loct.getX();
					int h = ((Rectangle)rect).getHeight() + loct.getY();
					if (w >maxRx)
					{
						maxRx=w;
					}
					if (h > maxRy)
						maxRy=h;
				}
				else if (tester instanceof Outline)
				{
					Location loct= onOutline((Outline) tester);
					if (loct.getX()<minLx)
					{
						minLx = loct.getX();
					}
					if (loct.getY()<minLy)
					{
						minLx= loct.getY();
					}
					Shape shape2=loct.getShape();
					int w=0;
					int h =0;
					if (shape2 instanceof Rectangle)
					{
						 w= ((Rectangle)shape2).getWidth() + loct.getX();
						 h = ((Rectangle)shape2).getHeight() + loct.getY();
					}
					else if (shape2 instanceof Circle)
					{
						int r = ((Circle)shape2).getRadius();
						topLx = x-r;
						topLy = y-r;
						botLx= x+r;
						botLy=y+r; //THIS LEADS NO WHERE!!!!!!



					}
					if (w >maxRx)
					{
						maxRx=w;
					}
					if (h > maxRy)
						maxRy=h;
					if (botLx > maxRx){
						maxRx=botLx;
					}
					if(botLy>maxRy){
						maxRy=botLy;

					}
				}
				else if (tester instanceof Fill)
				{
					Shape newgroup = ((Fill) tester).getShape();

					Location pls = onGroup((Group)newgroup);

					Rectangle test2  = (Rectangle)pls.getShape();
					int newBotLx = test2.getWidth() + l.getX();
					int newBotLy = test2.getHeight()+l.getY();
					int hihi=32;
					if (newBotLx > maxRx){
						maxRx=newBotLx;
					}
					if (newBotLy > maxRy){
						maxRy=newBotLy;
					}
				}



			}

			int hi = 1;

		}
		if (minLx<0)
		{
			return new Location(minLx,minLy,new Rectangle(maxRx,maxRy));
		}
		else {
			int newW = maxRx - minLx;
			int newH = maxRy - minLy;
			return new Location(minLx, minLy, new Rectangle(newW, newH));
		}

	}

	@Override
	public Location onLocation(final Location l) {
		final int x = l.getX();
		final int y = l.getY();
		Shape shape = l.getShape();
		//Rectangle r = (Rectangle) shape;
		//return new Location(x,y,new Rectangle(r.getWidth(),r.getHeight()));
		if (shape instanceof Group){

			Location loc = onGroup((Group) shape);
			Rectangle rect = (Rectangle)loc.getShape();
			int newx= rect.getWidth();
			int newy=rect.getHeight();

			int h = newy-loc.getY();
			int w=newx-loc.getY();
			Location ans =  new Location(loc.getX()+x,loc.getY()+y,new Rectangle(w,h));

			return ans;

		}

		return new Location (x,y,shape);
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();


		return new Location(0,0,new Rectangle(width,height)); //not sure about the location
	}

	@Override
	public Location onStroke(final Stroke c) {
		Shape shape = c.getShape();
		return new Location(0,0,shape); //maybe work? Stroke not implimented
	}

	@Override
	public Location onOutline(final Outline o) {
		Shape shape = o.getShape();
		//return new Location(0,0,new Rectangle(r.getWidth(),r.getHeight()));
		return new Location(0,0,shape);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		int minLx =0;
		int minLy =0;
		int maxRx=0;
		int maxRy=0;

		for (int i =0;i<points.size();i+=1)
		{
			int x =points.get(i).getX();
			int y = points.get(i).getY();
			if(i==0)
			{
				minLx=points.get(i).getX();
				minLy=points.get(i).getY();
				maxRx=points.get(i).getX();
				maxRy=points.get(i).getY();
			}
			else
			{
				if(minLx > points.get(i).getX())
				{
					minLx = points.get(i).getX();
				}
				if(minLy > y)
				{
					minLy = points.get(i).getY();
				}
				if(maxRx<points.get(i).getX())
				{
					maxRx=points.get(i).getX();
				}
				if (maxRy<points.get(i).getY())
				{
					maxRy=points.get(i).getY();
				}
			}
		}
		int w = maxRx-minLx;
		int h = maxRy-minLy;



		return new Location(minLx,minLy,new Rectangle(w,h)); //probably needs some work
	}
}
