package edu.luc.etl.cs313.android.shapes.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.List;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Size implements Visitor<Integer> {

	// TODO entirely your job


	//onPolygon, onCircle and onRectangle methods return 1
	@Override
	public Integer onPolygon(final Polygon p) {
		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) {
		return 1;
	}

	//onGroup must iterate through each object within the group
	// and assign that object to its respective onX() method
	@Override
	public Integer onGroup(final Group g) {

		/*
		This is broken right now, I'm trying to get testGroupMiddle to work
		 */

		//return shape count
		int j=0;

		//ArrayList<? extends Shape> l = new ArrayList<>(g.getShapes());

		for(Shape l: g.getShapes()){


			if(l instanceof Group){
				j+= onGroup((Group) l);
			}
			else if(l instanceof Rectangle || l instanceof Circle || l instanceof Polygon){
				j++;
			}
			else if(l instanceof Outline){
				j += onOutline((Outline) l);
			}
			else if(l instanceof Fill){
				j += onFill((Fill) l);
			}
			else if(l instanceof Location){
				j += onLocation((Location) l);
			}
			else if(l instanceof Stroke){
				j += onStroke((Stroke) l);
			}
		}
		return j;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		return 1;
	}


	//onOutline, onFill, onLocation and onStroke must check for
	// the object they manipulate. This code is essentially the same
	// as the conditionals within the onGroup method.
	@Override
	public Integer onOutline(final Outline o) {

		//we will getShape into Shape q
		Shape q = o.getShape();

		//then we will check q's type
		if(q instanceof Rectangle){
			return onRectangle((Rectangle) q);
		}
		else if(q instanceof Circle){
			return onCircle((Circle) q);
		}
		else if(q instanceof Polygon){
			return onPolygon((Polygon) q);
		}
		else if(q instanceof Group){
			return onGroup((Group) q);
		}
		else return null;
	}

	@Override
	public Integer onFill(final Fill c) {

		//we will getShape into Shape q
		Shape q = c.getShape();

		//then we will check q's type
		if(q instanceof Rectangle){
			return onRectangle((Rectangle) q);
		}
		else if(q instanceof Circle){
			return onCircle((Circle) q);
		}
		else if(q instanceof Polygon){
			return onPolygon((Polygon) q);
		}
		else if(q instanceof Group){
			return onGroup((Group) q);
		}
		else return null;
	}

	@Override
	public Integer onLocation(final Location l) {

		//we will getShape into Shape q
		Shape q = l.getShape();

		//then we will check q's type
		if(q instanceof Rectangle){
			return onRectangle((Rectangle) q);
		}
		else if(q instanceof Circle){
			return onCircle((Circle) q);
		}
		else if(q instanceof Polygon){
			return onPolygon((Polygon) q);
		}
		else if(q instanceof Group){
			return onGroup((Group) q);
		}
		else if (q instanceof Stroke){
			return onStroke((Stroke) q);
		}
		else if (q instanceof Fill){
			return onFill((Fill) q);
		}
		else if(q instanceof Outline){
			return onOutline((Outline) q);
		}
		else return null;

	}

	@Override
	public Integer onStroke(final Stroke c) {

		//we will getShape into Shape q
		Shape q = c.getShape();

		//then we will check q's type
		if(q instanceof Rectangle){
			return onRectangle((Rectangle) q);
		}
		else if(q instanceof Circle){
			return onCircle((Circle) q);
		}
		else if(q instanceof Polygon){
			return onPolygon((Polygon) q);
		}
		else if(q instanceof Group){
			return onGroup((Group) q);
		}
		else if(q instanceof Fill){
			return onFill((Fill) q);
		}
		else if(q instanceof Outline){
			return onOutline((Outline) q);
		}
		else return null;
	}
}
